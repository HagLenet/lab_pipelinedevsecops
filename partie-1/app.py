import re
from flask import Flask, render_template, request

app = Flask(__name__)

def addition(*args):
    return sum(args)

def subtraction(*args):
    result = args[0]
    for num in args[1:]:
        result -= num
    return result

def multiplication(*args):
    result = 1
    for num in args:
        result *= num
    return result

def division(*args):
    result = args[0]
    for num in args[1:]:
        if num != 0:
            result /= num
    return result

def evaluate_expression(expr):
    # Supprimer les espaces
    expr = expr.replace(' ', '')
    
    # Opérateurs pris en charge et leur priorité
    operators = {'+': 1, '-': 1, '*': 2, '/': 2}
    
    # Pile pour les opérateurs et les nombres
    operator_stack = []
    number_stack = []

    # Fonction pour résoudre les opérations en attente sur la pile
    def resolve_operation():
        operator = operator_stack.pop()
        num2 = number_stack.pop()
        num1 = number_stack.pop()
        if operator == '+':
            number_stack.append(num1 + num2)
        elif operator == '-':
            number_stack.append(num1 - num2)
        elif operator == '*':
            number_stack.append(num1 * num2)
        elif operator == '/':
            if num2 == 0:
                raise ValueError("Division by zero")
            number_stack.append(num1 / num2)

    i = 0
    while i < len(expr):
        if expr[i].isdigit():  # Si c'est un chiffre
            j = i
            while j < len(expr) and (expr[j].isdigit() or expr[j] == '.'):
                j += 1
            number_stack.append(float(expr[i:j]))
            i = j
        elif expr[i] in operators:  # Si c'est un opérateur
            while (operator_stack and operators[expr[i]] <= operators[operator_stack[-1]]):
                resolve_operation()
            operator_stack.append(expr[i])
            i += 1
        else:
            raise ValueError("Invalid character in expression")

    while operator_stack:
        resolve_operation()

    return number_stack[0] if number_stack else "Invalid expression"


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/calculate', methods=['POST'])
def calculate():
    expression = request.form['expression']
    result = evaluate_expression(expression)
    return render_template('result.html', expression=expression, result=result)

if __name__ == '__main__':
    app.run(debug=True)

