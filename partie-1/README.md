Projet 1 : Developpement d'une calcultrice suivant le cycle en V de developpement d'un logiciel.
 
Application web ecrit en python avec le framework flask

Il s'agit d'une application monolithique.

La calculatrice effectue les operations de base (addition +, soustraction -, multiplication * et division /)

Outils à installer :

Python3 --> sudo apt install python3

pip3 --> sudo apt install python3-pip

flask --> pip3 install flask

locust --> pip3 install locust 


Lancement de l'appli :

python3 app.py

Ensuite ouvrier le navigateur : localhost:5000

Pour les tests :

test unitaire --> python3 test_calculator.py

test integration --> python3 test_integration.py

test de charge --> locust -f test_2charge.py --users nb_users

Ensuite aller dans le navigateur : localhost:8089 et lancer le test de charge. 
