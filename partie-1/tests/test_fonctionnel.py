import unittest
from selenium import webdriver

class TestFunctional(unittest.TestCase):
    def setUp(self):
        # Utilisation du pilote pour Opera
        # Remplacez le chemin vers le pilote par le chemin approprié sur votre système
        self.opera_driver_path = "/chemin/vers/le/driver/opera"
        self.opera_options = webdriver.ChromeOptions()
        self.opera_options.binary_location = "/chemin/vers/opera/binaire"
        self.opera_driver = webdriver.Opera(executable_path=self.opera_driver_path, options=self.opera_options)
        self.opera_driver.get("http://localhost:5000")  # URL de votre application

        # Utilisation du pilote pour Firefox
        # Remplacez le chemin vers le pilote par le chemin approprié sur votre système
        self.firefox_driver_path = "/chemin/vers/le/driver/firefox"
        self.firefox_driver = webdriver.Firefox(executable_path=self.firefox_driver_path)
        self.firefox_driver.get("http://localhost:5000")  # URL de votre application

    def test_calculator_with_opera(self):
        expression_input = self.opera_driver.find_element_by_name("expression")
        expression_input.send_keys("2+3*4-5/2")
        calculate_button = self.opera_driver.find_element_by_css_selector("button[type='submit']")
        calculate_button.click()
        result_element = self.opera_driver.find_element_by_xpath("//p[contains(text(), 'Result:')]")
        result_text = result_element.text
        self.assertEqual(result_text, "Result: 10.5")

    def test_calculator_with_firefox(self):
        expression_input = self.firefox_driver.find_element_by_name("expression")
        expression_input.send_keys("2+3*4-5/2")
        calculate_button = self.firefox_driver.find_element_by_css_selector("button[type='submit']")
        calculate_button.click()
        result_element = self.firefox_driver.find_element_by_xpath("//p[contains(text(), 'Result:')]")
        result_text = result_element.text
        self.assertEqual(result_text, "Result: 10.5")

    def tearDown(self):
        self.opera_driver.quit()
        self.firefox_driver.quit()

if __name__ == '__main__':
    unittest.main()

