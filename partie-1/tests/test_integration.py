#Le test d'intégration consiste à vérifier la bonne interaction et la communication entre les différentes composantes d'un système, 
#une fois qu'elles ont été unitairement testées et jugées conformes. 

#Dans le contexte de l'application de calculatrice, il s'agit de vérifier que toutes les fonctionnalités individuelles (par exemple l'addition,
#la soustraction, la multiplication) se combinent correctement et se comportent comme prévu lorsqu'elles sont utilisées ensemble.


import unittest
from app import app

class TestIntegration(unittest.TestCase):
    def setUp(self):
        app.testing = True
        self.app = app.test_client()

    def test_integration(self):
        expressions = [
            "2+3*4-5/2",
            "6*8-4+2/2",
            "10/2+5*3-1",
            "100-50*2/5+10",
            "5*7-3+18/2",
            "12/3*4+6-8",
            "7+8/2*4-3",
            "9-3*2/4+5",
        ]

        for expr in expressions:
            response = self.app.post('/calculate', data={'expression': expr})
            self.assertEqual(response.status_code, 200)
            self.assertIn(b'Result:', response.data)

if __name__ == '__main__':
    unittest.main()

