#Le test de régression est effectué pour vérifier si les nouvelles modifications ou ajouts à un système n'ont pas introduit de
#régressions ou de défauts dans les fonctionnalités existantes. 

#Dans le contexte de notre application de calculatrice, cela consisterait à vérifier si une modification apportée à l'opération 
#d'addition n'a pas affecté négativement les fonctionnalités de soustraction, multiplication et division qui avaient été précédemment testées et approuvées.


import unittest
from app import app, evaluate_expression

class TestRegression(unittest.TestCase):
    def test_addition(self):
        result = evaluate_expression("2+3+4")
        self.assertEqual(result, 9)

    def test_subtraction(self):
        result = evaluate_expression("10-5-3")
        self.assertEqual(result, 2)

    def test_multiplication(self):
        result = evaluate_expression("2*3*4")
        self.assertEqual(result, 24)

    def test_division(self):
        result = evaluate_expression("10/2/5")
        self.assertEqual(result, 1)

    def test_valid_expression(self):
        result = evaluate_expression("2+3*4-5/2")
        self.assertEqual(result, 10.5)

    def test_invalid_expression(self):
        result = evaluate_expression("2+3*")
        self.assertEqual(result, "Invalid expression")

if __name__ == '__main__':
    unittest.main()

