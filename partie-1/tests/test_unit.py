#Le test unitaire est un processus de vérification d'une unique unité de logiciel, elle-même définie comme étant la plus petite partie non divisible d'un code source.


import unittest
from app import app, evaluate_expression

class TestCalculator(unittest.TestCase):
    def setUp(self):
        app.testing = True
        self.app = app.test_client()

    def test_addition(self):
        result = evaluate_expression("2+3+4")
        self.assertEqual(result, 9)

    def test_subtraction(self):
        result = evaluate_expression("10-5-3")
        self.assertEqual(result, 2)

    def test_multiplication(self):
        result = evaluate_expression("2*3*4")
        self.assertEqual(result, 24)

    def test_division(self):
        result = evaluate_expression("10/2/5")
        self.assertEqual(result, 1)

    def test_invalid_expression(self):
        result = evaluate_expression("2+3*")
        self.assertEqual(result, "Invalid expression")

if __name__ == '__main__':
    unittest.main()

