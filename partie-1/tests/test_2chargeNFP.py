from locust import HttpUser, between, task

class MyUser(HttpUser):
    wait_time = between(5, 9)

    @task
    def my_task(self):
        self.client.post("/calculate", json={"expression": "2+3+4"})  # Exemple de requête POST à votre application

