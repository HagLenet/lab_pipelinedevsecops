# Microservice division
from flask import Flask, request, jsonify, render_template
  
app = Flask(__name__)

@app.route('/divide', methods=['GET', 'POST'])
def divide():
    if request.method == 'GET':
        return render_template('divide_template.html')
    elif request.method == 'POST':
        data = request.json
        if 'expression' not in data:
            return jsonify({'error': 'Expression missing'}), 400

        expression = data['expression']
        result = evaluate(expression)
        return jsonify({'result': result})

def evaluate(expression):
    parts = expression.split('/')
    if len(parts) < 2:
        return jsonify({'error': 'Invalid division expression'}), 400
    result = float(parts[0]) / float(parts[1])
    return result

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5004)

