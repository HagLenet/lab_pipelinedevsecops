import os
import sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import unittest
from unittest.mock import patch, MagicMock
from flask import Flask
from appUixms import evaluate_expression

class TestEvaluateExpression(unittest.TestCase):
    def setUp(self):
        self.app = Flask(__name__)

    def test_evaluate_expression_addition(self):
        with self.app.test_request_context('/evaluate', method='POST', json={'expression': '5+3'}):
            response = evaluate_expression()
            self.assertEqual(response.json, {'result': 8})

    def test_evaluate_expression_subtraction(self):
        with self.app.test_request_context('/evaluate', method='POST', json={'expression': '5-3'}):
            response = evaluate_expression()
            self.assertEqual(response.json, {'result': 2})

    def test_evaluate_expression_multiplication(self):
        with self.app.test_request_context('/evaluate', method='POST', json={'expression': '5*3'}):
            response = evaluate_expression()
            self.assertEqual(response.json, {'result': 15})

    def test_evaluate_expression_division(self):
        with self.app.test_request_context('/evaluate', method='POST', json={'expression': '6/3'}):
            response = evaluate_expression()
            self.assertEqual(response.json, {'result': 2})

    @patch('app.requests.post')
    def test_evaluate_expression_division_by_zero(self, mock_post):
        mock_post.return_value.status_code = 200
        mock_post.return_value.json.return_value = {'result': 0}
        with self.app.test_request_context('/evaluate', method='POST', json={'expression': '6/0'}):
            response = evaluate_expression()
            self.assertEqual(response.json, {'error': 'Division by zero'})

if __name__ == '__main__':
    unittest.main()

