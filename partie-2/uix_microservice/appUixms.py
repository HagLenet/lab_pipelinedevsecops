from flask import Flask, request, jsonify, render_template
import requests

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/evaluate', methods=['POST'])
def evaluate_expression():
    data = request.json
    if 'expression' not in data:
        return jsonify({'error': 'Expression missing'}), 400
    
    expression = data['expression']
    result = evaluate(expression)
    return jsonify({'result': result})

def evaluate(expression):
    operators = ['+', '-', '*', '/']
    parts = []
    current_number = ''
    
    for char in expression:
        if char.isdigit() or char == '.':
            current_number += char
        elif char in operators:
            parts.append(float(current_number))
            parts.append(char)
            current_number = ''
    
    if current_number:
        parts.append(float(current_number))
    
    result = parts[0]
    operator = None
    
    for part in parts[1:]:
        if isinstance(part, str) and part in operators:
            operator = part
        elif isinstance(part, float):
            if operator == '+':
                addition_result = requests.post('http://addition-ms:5001/add', json={'expression': str(part)})
                if addition_result.status_code == 200:
                    result += addition_result.json().get('result', 0)
                else:
                    return jsonify({'error': 'Failed to get result from addition service'}), 500
            elif operator == '-':
                subtraction_result = requests.post('http://subtract-ms:5002/subtract', json={'expression': str(part)})
                if subtraction_result.status_code == 200:
                    result -= subtraction_result.json().get('result', 0)
                else:
                    return jsonify({'error': 'Failed to get result from subtraction service'}), 500
            elif operator == '*':
                multiplication_result = requests.post('http://multiply-ms:5003/multiply', json={'expression': str(part)})
                if multiplication_result.status_code == 200:
                    result *= multiplication_result.json().get('result', 1)
                else:
                    return jsonify({'error': 'Failed to get result from multiplication service'}), 500
            elif operator == '/':
                division_result = requests.post('http://divide-ms:5004/divide', json={'expression': str(part)})
                if division_result.status_code == 200:
                    divisor = division_result.json().get('result', 1)
                    if divisor != 0:
                        result /= divisor
                    else:
                        return jsonify({'error': 'Division by zero'}), 400
                else:
                    return jsonify({'error': 'Failed to get result from division service'}), 500
    return result

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

