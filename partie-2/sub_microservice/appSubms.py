# Microservice subtraction
from flask import Flask, request, jsonify, render_template
  
app = Flask(__name__)

@app.route('/subtract', methods=['GET', 'POST'])
def subtract():
    if request.method == 'GET':
        return render_template('subtract_template.html')
    elif request.method == 'POST':
        data = request.json
        if 'expression' not in data:
            return jsonify({'error': 'Expression missing'}), 400

        expression = data['expression']
        result = evaluate(expression)
        return jsonify({'result': result})

def evaluate(expression):
    parts = expression.split('-')
    result = float(parts[0]) - sum(float(part) for part in parts[1:])
    return result

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5002)

