# Microservice addition
from flask import Flask, request, jsonify, render_template
  
app = Flask(__name__)

@app.route('/add', methods=['GET', 'POST'])
def add():
    if request.method == 'GET':
        return render_template('addition_template.html')
    elif request.method == 'POST':
        data = request.json
        if data is None or 'expression' not in data:
            return jsonify({'error': 'Expression missing or not provided in JSON format'}), 400

        expression = data['expression']
        result = evaluate(expression)
        return jsonify({'result': result})

def evaluate(expression):
    parts = expression.split('+')
    result = sum(float(part) for part in parts)
    return result

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)

