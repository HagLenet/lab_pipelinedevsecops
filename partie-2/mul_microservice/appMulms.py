# Microservice multiplication
from flask import Flask, request, jsonify, render_template
  
app = Flask(__name__)

@app.route('/multiply', methods=['GET', 'POST'])
def multiply():
    if request.method == 'GET':
        return render_template('multiply_template.html')
    elif request.method == 'POST':
        data = request.json
        if 'expression' not in data:
            return jsonify({'error': 'Expression missing'}), 400

        expression = data['expression']
        result = evaluate(expression)
        return jsonify({'result': result})

def evaluate(expression):
    parts = expression.split('*')
    result = 1
    for part in parts:
        result *= float(part)
    return result

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5003)

